FROM node:latest
WORKDIR /src
COPY /api-rest ./
RUN npm install
EXPOSE 3000
CMD [ "node" , "./bin/www" ]
