/*
 * Product Model Data manager
 */
 import redis from 'redis';
 import request from 'request';
 import bluebird from  'bluebird';
 import "core-js/stable";
 import "regenerator-runtime/runtime";
 /*
  * Define const
  */
 const urlExternalApi = 'https://simple.ripley.cl/api/v2';
 const redisUrl = process.env.REDISCLOUD_URL;
 const REDIS_EXPIRATION = 60;
 const client = redis.createClient(redisUrl);

 //Insert on Redis
  export const insertRedis = (key, value) => {
     return client.set(`${key}`, value , 'EX', REDIS_EXPIRATION);
  }

 //Get from redis
 export const getFromRedis = (id) => {
   bluebird.promisifyAll(redis);
   //Return promise
   return new Promise((resolve, reject) => {
     client.getAsync(`${id}`).then((response) => {
       if(response){
         return resolve(response);
       } else {
         return resolve(false);
       }
     });
   });
 }//End function

//Get from external Api
 export const getFromeExternalApi = (id,insert=true) => {
   //Return Promise
   return new Promise((resolve, reject) => {
     let request_url = `${urlExternalApi}/products/${id}`;
     request(request_url, (error, response, body) => {
       if(insert){ insertRedis(id, body); }
       return resolve(body);
     });
  });
} //End function

export const getSetOfProducts = async () => {
  let listProducts = [];
  let redisList = [];
  let set = [2000372359445,2000373457263,2000373717374,2000372061959,2000371897887,
             2000372098481,2000373313187,2000371959264,2000371969461,2000370982089,
             2000370623418,2000372026347];

  let response = await getFromRedis('catalog');

  if(response){
    response = JSON.parse(response);
    //Make right object
    for(let product of response){
      listProducts.push(JSON.parse(product));
    }
  }

  //Return Promise
  return new Promise(async (resolve, reject) => {
    if(!listProducts.length){
      for(let id of set){
        let product = await getFromRedis(id);
        if(product){
          listProducts.push(JSON.parse(product));
          redisList.push(product);
        } else {
          product = await getFromeExternalApi(id, false);
          listProducts.push(JSON.parse(product));
          redisList.push(product);
        }
      }
      //save catalog on redis
      client.set('catalog', JSON.stringify(redisList) , 'EX', REDIS_EXPIRATION);
    }
    return resolve(listProducts);
  });
}
