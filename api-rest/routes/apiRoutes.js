import { Router } from 'express';
import { getProductController, getCatalog } from '../controllers/apiController';
var apiRoutes = Router();
/*
 * Set Api routes Here
 */
apiRoutes.get('/product/:id', getProductController);
apiRoutes.get('/products', getCatalog);

export default apiRoutes;
