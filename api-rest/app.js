/*
 *   Import's with ES6
 */
import  express from 'express';
import  path from 'path';
import  cors from 'cors';
import  apiRoutes from './routes/apiRoutes';
/*
 * App use
 */
const app = express();
app.use(cors({
    'allowedHeaders': ['Content-Type'],
    'exposedHeaders': ['Content-Type'],
    'origin': '*',
    'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
    'preflightContinue': false
}));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

//Use Api Routes
app.use('/', apiRoutes);

module.exports = app;
