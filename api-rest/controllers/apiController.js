/*
 *  Api controllers
 */
 import "core-js/stable";
 import "regenerator-runtime/runtime";
 import { getFromRedis,
          getFromeExternalApi,
          getSetOfProducts } from '../models/ProductModel';

/*
 * Controller getSingleProduct
 */
export const getProductController = async (req, res) => {
  //Validation required param
  if(!req.params.id){
    res.send({data: {}, error: true});
    return false; //exit
  }
  //Get from redis
  let product = await getFromRedis(req.params.id);
  if(!product){ //Get from external api
    product = await getFromeExternalApi(req.params.id);
    res.send({data: JSON.parse(product), error: false });
  } else {
    res.send({data: JSON.parse(product), error: false });
  }
}; //End function

/*
 * Controller getSetOfProducts
 */
export const getCatalog = async (req, res) => {
  let products = await getSetOfProducts();
  res.send({data: products, error: false });
};
